package pdbcreate

import (
	"ALKA-backend/db/pdbcon"
	"ALKA-backend/pdbresponse"
	"ALKA-backend/pdbstruct/pdbaddress"
	"ALKA-backend/pdbstruct/pdbduitkuDetail"
	"ALKA-backend/pdbstruct/pdbinquiry"
	"ALKA-backend/pdbstruct/pdbitem"
	"ALKA-backend/pdbstruct/pdborder"
	"ALKA-backend/pdbstruct/pdbpayment"
	"ALKA-backend/pdbstruct/pdbproduct"
	"ALKA-backend/pdbstruct/pdbsetting"
	"ALKA-backend/pdbstruct/pdbuser"
	decimal "ALKA-backend/shopspring-decimal"
	"net/http"

	"github.com/labstack/echo"
)

// Createdb untuk apaß
func Createdb(c echo.Context) error {
	db := pdbcon.Connecting()
	defer db.Close()

	db.AutoMigrate(pdbitem.Item{})
	db.AutoMigrate(pdbitem.Picture{})
	db.AutoMigrate(pdbinquiry.Inquiry{})
	db.AutoMigrate(pdbsetting.Setting{})
	db.AutoMigrate(pdbuser.User{})
	db.AutoMigrate(pdbuser.AdminPrivilege{})
	db.AutoMigrate(pdbuser.Message{})
	db.AutoMigrate(pdbuser.Read{})
	db.AutoMigrate(pdbaddress.Address{})
	db.AutoMigrate(pdborder.Order{})
	db.AutoMigrate(pdbduitkuDetail.DuitkuDetail{})
	db.AutoMigrate(pdbpayment.PaymentDetail{})
	db.AutoMigrate(pdbproduct.Product{})

	out := pdbresponse.SetResponse(true, "Create table success", map[string]string{
		"create": "success",
	})

	return c.JSON(http.StatusOK, out)

}

//InitValueParamDB default parameter
func InitValueParamDB(c echo.Context) error {
	item1 := new(pdbitem.Item)
	item2 := new(pdbitem.Item)
	setting := new(pdbsetting.Setting)
	setting2 := new(pdbsetting.Setting)
	setting3 := new(pdbsetting.Setting)
	testSettings := []pdbsetting.Setting{}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Find(&testSettings).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error InitValueParamDB", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	if len(testSettings) == 0 {

		item1Price := decimal.NewFromFloat(2000000)
		item1AdminPrice := decimal.NewFromFloat(50000)
		item1DeliveryPrice := decimal.NewFromFloat(0)
		item2Price := decimal.NewFromFloat(3000000)
		item2AdminPrice := decimal.NewFromFloat(50000)
		item2DeliveryPrice := decimal.NewFromFloat(0)

		item1.ItemPrice = item1Price
		item1.ItemName = "20-25 KG"
		item1.ItemCategory = "KAMBING"
		item1.ItemAdminPrice = item1AdminPrice
		item1.ItemDeliveryPrice = item1DeliveryPrice
		item1.HaveCancelDeadline = true
		db.NewRecord(&item1)
		db.Create(&item1)
		item2.ItemPrice = item2Price
		item2.ItemName = "30-35 KG"
		item2.ItemCategory = "KAMBING"
		item2.ItemAdminPrice = item2AdminPrice
		item2.ItemDeliveryPrice = item2DeliveryPrice
		item2.HaveCancelDeadline = true
		db.NewRecord(&item2)
		db.Create(&item2)
		setting.SettingName = "LebaranMonth"
		setting.SettingValue = "8"
		setting2.SettingName = "DeadlineMonth"
		setting2.SettingValue = "2"
		setting3.SettingName = "BannerPicIds"
		setting3.SettingValue = ""
		db.NewRecord(&setting)
		db.Create(&setting)
		db.NewRecord(&setting2)
		db.Create(&setting2)
		db.NewRecord(&setting3)
		db.Create(&setting3)

	}

	out := pdbresponse.SetResponse(true, "Create parameter success", map[string]string{
		"create": "success",
	})

	return c.JSON(http.StatusOK, out)

}
