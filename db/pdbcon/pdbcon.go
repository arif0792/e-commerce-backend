package pdbcon

import (
	"ALKA-backend/assets/keys"
	"fmt"

	"github.com/jinzhu/gorm"
)

// Connecting to connect to database
func Connecting() *gorm.DB {
	var err error
	var db *gorm.DB

	if keys.Env == "DEV" {
		db, err = gorm.Open(keys.DBType, keys.DBAddressLocal)
	} else {
		db, err = gorm.Open(keys.DBType, keys.DBAddress)
	}

	if err != nil {
		fmt.Println("failed to connect database = " + err.Error())
	}

	return db
}
