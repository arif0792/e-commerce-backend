package main

import (
	"ALKA-backend/assets/keys"
	"ALKA-backend/db/pdbcon"
	"ALKA-backend/db/pdbcreate"
	"ALKA-backend/path/pdbaccount"
	"ALKA-backend/path/pdbaddress"
	"ALKA-backend/path/pdbduitku"
	"ALKA-backend/path/pdbinquiry"
	"ALKA-backend/path/pdborder"
	"ALKA-backend/path/pdbparameter"
	"ALKA-backend/path/pdbproduct"
	"ALKA-backend/pdbresponse"
	orderStruct "ALKA-backend/pdbstruct/pdborder"
	paymentStruct "ALKA-backend/pdbstruct/pdbpayment"
	"ALKA-backend/pdbstruct/pdbuser"
	"fmt"
	"os"
	"time"

	"github.com/jinzhu/now"
	"github.com/parnurzeal/gorequest"

	"github.com/jasonlvhit/gocron"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {

	e := echo.New()

	// CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"}, // you should get to know what is this
		AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	}))

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	versioning := "/panen/v1"
	// version := e.Group(versioning)
	parameter := e.Group(versioning + "/parameter")

	address := e.Group(versioning + "/address")
	account := e.Group(versioning + "/account")
	order := e.Group(versioning + "/order")
	product := e.Group(versioning + "/product")
	inquiry := e.Group(versioning + "/inquiry")
	paymentGateway := e.Group(versioning + "/paymentgateway")

	e.GET("/admin/createdb", pdbcreate.Createdb)
	e.GET("/admin/createdefparam", pdbcreate.InitValueParamDB)

	//parameter
	parameter.GET("/itemsByCategory", pdbparameter.GetParameterItemsByCategory)
	parameter.GET("/items", pdbparameter.GetParameterItems)
	parameter.POST("/items", pdbparameter.PostParameterItems)
	parameter.GET("/itemPictures", pdbparameter.GetItemPictures)
	parameter.POST("/itemPictures", pdbparameter.PostItemPictures)
	parameter.DELETE("/itemPicture", pdbparameter.DeleteItemPicture)
	parameter.DELETE("/item", pdbparameter.DeleteParameterItem)
	parameter.GET("/lebaran", pdbparameter.GetParameterSettingLebaranMonth)
	parameter.GET("/deadline", pdbparameter.GetParameterSettingDeadlineMonth)
	parameter.GET("/banner", pdbparameter.GetParameterSettingBannerPics)
	parameter.POST("/setting", pdbparameter.PostParameterSetting)

	//inquiry
	inquiry.POST("/inquiry", pdbinquiry.PostInquiry)
	inquiry.GET("/inquiries", pdbinquiry.GetInquiries)

	//account user
	account.POST("/userNameCheck", pdbaccount.UsernameCheck)
	account.POST("/emailCheck", pdbaccount.EmailCheck)
	account.POST("/phoneNumberCheck", pdbaccount.PhoneNumberCheck)
	account.POST("/passwordCheck", pdbaccount.PasswordCheck)
	account.POST("/account", pdbaccount.SaveOrUpdateAccount)
	account.POST("/changePassword", pdbaccount.ChangePasswordAccount)
	account.PUT("/updateFcmToken", pdbaccount.UpdateFCMToken)
	account.GET("/account", pdbaccount.GetUserByID)
	account.GET("/accountByName", pdbaccount.GetUsersByName)
	account.GET("/accountByNameCount", pdbaccount.GetUsersCountByName)
	account.POST("/login", pdbaccount.Login)
	account.POST("/message", pdbaccount.SaveOrUpdateMessage)
	account.GET("/messages", pdbaccount.GetMessagesByUserID)
	account.POST("/read", pdbaccount.SaveOrUpdateRead)
	account.GET("/reads", pdbaccount.GetReadsByUserID)

	//addresses
	address.GET("/addresses", pdbaddress.GetAddressesByUserID)

	//address
	address.DELETE("/address", pdbaddress.DeleteAddressByAddressID)
	address.POST("/address", pdbaddress.SaveOrUpdateAddress)

	//orders
	order.GET("/orders", pdborder.GetOrdersByUserID)
	order.GET("/ordersRefundByIDCount", pdborder.GetRefundOrdersByIDCount)
	order.GET("/ordersRefundByID", pdborder.GetRefundOrdersByID)
	order.GET("/ordersOverDueByIDCount", pdborder.GetOverDueOrdersByIDCount)
	order.GET("/ordersOverDueByID", pdborder.GetOverDueOrdersByID)
	order.GET("/ordersHistoryByIDCount", pdborder.GetHistoryOrdersByIDCount)
	order.GET("/ordersHistoryByID", pdborder.GetHistoryOrdersByID)

	//order
	order.POST("/order", pdborder.SaveOrUpdateOrder)
	order.DELETE("/order", pdborder.DeleteOrderByID)
	// order.PUT("/cancelOrder", pdborder.CancelOrder)

	//products
	product.GET("/products", pdbproduct.GetProductsByOrderID)
	product.GET("/productsByKelurahan", pdbproduct.GetProductsByKelurahan)
	product.GET("/productsByKelurahanCount", pdbproduct.GetProductsByKelurahanCount)
	product.GET("/productsByPotong", pdbproduct.GetProductsByPotong)
	product.GET("/productsByPotongCount", pdbproduct.GetProductsByPotongCount)

	//product
	product.POST("/products", pdbproduct.SaveOrUpdateProducts)

	//payment gateway
	paymentGateway.GET("/init", pdbduitku.InitDuitkuPayment)
	paymentGateway.POST("/callback", pdbduitku.SaveOrUpdateCallBack)

	//scheduler
	gocron.Every(1).Day().At("12:00").Do(SchedulerNotif)
	gocron.Start()

	if keys.Env == "DEV" {
		e.Start(":1323") //DEV
	} else {
		e.Start(":" + os.Getenv("PORT")) // Heroku
	}
}

func SchedulerNotif() error {

	fmt.Println("masuk service SchedulerNotif")
	orders := []orderStruct.Order{}
	paymentDetails := []paymentStruct.PaymentDetail{}
	dayDate := time.Now().Day()
	due := now.BeginningOfMonth()

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Model(&orders).Where("DAY(order_date) = ? and status = 1 and is_active = 1", &dayDate).Find(&orders).Error; err != nil {
		fmt.Println("Error get order => " + err.Error())
	}

	for _, order := range orders {
		var count int
		if err := db.Model(&paymentDetails).Where("order_id = ?", &order.ID).Find(&paymentDetails).Count(&count).GetErrors(); len(err) > 0 {
			fmt.Println("Error get paymentDetails => " + err[0].Error())
		}

		if count < order.TotalInstallment && order.OrderDate.AddDate(0, count, 0).Before(due) {

			resp := new(pdbresponse.OutStruct)
			msg := new(pdbuser.Message)
			msg.From = "Admin"
			msg.UserID = order.UserID
			msg.Data = "Notif untuk segera melakukan pembayaran untuk bulan ini pada no Order #" + fmt.Sprint(order.ID) + ". Terima kasih."

			fmt.Println(keys.OwnAddress + "/panen/v1/account/message")

			gorequest.New().Post(keys.OwnAddress+"/panen/v1/account/message").
				Set("Content-Type", "application/json").
				Send(msg).EndStruct(&resp)

			if !resp.Success {
				fmt.Println("Error request msg => " + resp.Message)
			}

		}
	}

	return nil
}
