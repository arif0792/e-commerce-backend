package pdbresponse

// OutStruct : This is the template to out the message
type OutStruct struct {
	Success bool        `json:"success"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// SetResponse : set template of the return
func SetResponse(s bool, m string, d interface{}) OutStruct {
	out := OutStruct{
		Success: s,
		Message: m,
		Data:    d,
	}

	return out
}
