package pdborder

import (
	"ALKA-backend/assets/keys"
	"ALKA-backend/db/pdbcon"
	"ALKA-backend/pdbresponse"
	"ALKA-backend/pdbstruct/pdborder"
	"ALKA-backend/pdbstruct/pdbpayment"
	"ALKA-backend/pdbstruct/pdbproduct"
	"ALKA-backend/pdbstruct/pdbuser"
	decimal "ALKA-backend/shopspring-decimal"
	"encoding/json"

	"fmt"
	"net/http"
	"strconv"
	"time"

	fcm "github.com/NaySoftware/go-fcm"
	"github.com/jinzhu/gorm"
	"github.com/jinzhu/now"
	"github.com/labstack/echo"
)

func SaveOrUpdateOrder(c echo.Context) error {
	order := new(pdborder.Order)

	if err := c.Bind(order); err != nil {
		fmt.Println("Error Data Binding")
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	if order.ID == 0 {
		db.NewRecord(&order)

		if err := db.Create(&order).Error; err != nil {
			out := pdbresponse.SetResponse(false, "Error creating order", err.Error())
			return c.JSON(http.StatusInternalServerError, &out)
		}

		year := time.Now().Year()
		yearString := strconv.Itoa(year)
		yearForInsert := yearString[2:len(yearString)]
		//
		fourDigitOrder := fmt.Sprint(order.ID)
		lengthOrderID := len(fourDigitOrder)
		zeroID := ""
		for lengthOrderID < 4 {
			zeroID = zeroID + "0"
			lengthOrderID++
		}
		fourDigitOrder = zeroID + fourDigitOrder
		//
		num := 1
		//

		for i, product := range order.Products {
			//
			var twoDigitProduct string
			if len(strconv.Itoa(num)) != 2 {
				twoDigitProduct = "0" + strconv.Itoa(num)
			} else {
				twoDigitProduct = strconv.Itoa(num)
			}
			//
			product.ProductID = yearForInsert + fourDigitOrder + twoDigitProduct
			if err := db.Save(&product).Error; err != nil {
				out := pdbresponse.SetResponse(false, "Error creating order", err.Error())
				return c.JSON(http.StatusInternalServerError, &out)
			}
			order.Products[i].ProductID = product.ProductID
			num++
		}

	} else {
		fmt.Println(order.CreatedAt)
		if err := db.LogMode(true).Save(&order).GetErrors(); len(err) > 0 {
			out := pdbresponse.SetResponse(false, "Error update order", err)
			return c.JSON(http.StatusInternalServerError, &out)
		}
	}

	//fcm
	orderTemp := *order
	order.PaymentDetails = nil
	order.Products = nil
	jsonMsg, _ := json.Marshal(order)
	data := map[string]interface{}{
		"order": string(jsonMsg),
	}

	// var ids []string

	user := pdbuser.User{}
	if err := db.Table("users").Where("id = ?", &order.UserID).Find(&user).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetOrdersByUserId", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	// ids = []string{
	// 	user.FcmToken,
	// }

	f := fcm.NewFcmClient(keys.FcmServerKey)
	// f.NewFcmRegIdsMsg(ids, data)
	//change using topic / user
	f.NewFcmMsgTo("/topics/userId"+fmt.Sprint(user.ID), data)

	status, err := f.Send()
	if err == nil {
		status.PrintResults()
	} else {
		out := pdbresponse.SetResponse(false, "Error FCM", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	order.PaymentDetails = orderTemp.PaymentDetails
	order.Products = orderTemp.Products
	//endfcm

	out := pdbresponse.SetResponse(true, "Succesfully create/update order", &order)
	return c.JSON(http.StatusOK, &out)
}

func DeleteOrderByID(c echo.Context) error {
	orderID := c.QueryParam("orderId")
	order := new(pdborder.Order)

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Where("id = ?", &orderID).Find(&order).Delete(&order).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error DeleteOrderByID", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully DeleteOrderByID", &order)
	return c.JSON(http.StatusOK, &out)
}

func GetOrdersByUserID(c echo.Context) error {
	userID := c.QueryParam("userId")
	orders := []pdborder.Order{}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Model(&orders).Preload("Products").Preload("PaymentDetails", func(db *gorm.DB) *gorm.DB {
		return db.Order("payment_details.id DESC")
	}).Preload("PaymentDetails.DuitkuDetail", func(db *gorm.DB) *gorm.DB {
		return db.Order("duitku_details.id DESC")
	}).Order("id DESC").Where("user_id = ? and is_active =  ?", &userID, 1).Find(&orders).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetOrdersByUserId", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully get orders by userid", &orders)
	return c.JSON(http.StatusOK, &out)
}

func GetRefundOrdersByIDCount(c echo.Context) error {
	ID := c.QueryParam("id")
	ID = "%" + ID + "%"

	var count int

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Table("orders").Select("orders.*, users.first_name, users.last_name").Joins("inner join users on users.id = orders.user_id").Where("orders.user_id = users.id and orders.id LIKE ? and orders.status = 3 and orders.is_refund = 1 and orders.is_active =  ?", &ID, 1).Order("orders.id DESC").Count(&count).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetOrdersByID", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetOrdersByID", &count)
	return c.JSON(http.StatusOK, &out)
}

func GetRefundOrdersByID(c echo.Context) error {
	start, err := strconv.Atoi(c.QueryParam("start"))

	if err != nil {
		out := pdbresponse.SetResponse(false, "Error GetRefundOrdersByID", "Error GetRefundOrdersByID")
		return c.JSON(http.StatusOK, &out)
	}

	ID := c.QueryParam("id")
	orders := []OrdersJoinUsers{}
	ID = "%" + ID + "%"

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Table("orders").Select("orders.*, users.first_name, users.last_name").Joins("inner join users on users.id = orders.user_id").Where("orders.user_id = users.id and orders.id LIKE ? and orders.status = 3 and orders.is_refund = 1 and orders.is_active =  ?", &ID, 1).Order("orders.id DESC").Offset(start).Limit(5).Scan(&orders).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetOrdersByID", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	for i, order := range orders {

		if err := db.Where("order_id = ?", fmt.Sprint(order.ID)).Find(&orders[i].Products).GetErrors(); len(err) > 0 {
			out := pdbresponse.SetResponse(false, "Error GetRefundOrdersByID", err)
			return c.JSON(http.StatusInternalServerError, &out)
		}

		if err := db.Model(&orders[i].PaymentDetails).Preload("DuitkuDetail").Where("order_id = ?", fmt.Sprint(order.ID)).Find(&orders[i].PaymentDetails).GetErrors(); len(err) > 0 {
			out := pdbresponse.SetResponse(false, "Error GetRefundOrdersByID", err)
			return c.JSON(http.StatusInternalServerError, &out)
		}

	}

	out := pdbresponse.SetResponse(true, "Succesfully GetOrdersByID", &orders)
	return c.JSON(http.StatusOK, &out)
}

func GetOverDueOrdersByIDCount(c echo.Context) error {
	ID := c.QueryParam("id")
	ID = "%" + ID + "%"

	orders := []OrdersJoinUsers{}
	paymentDetails := []pdbpayment.PaymentDetail{}
	ordersReturn := []OrdersJoinUsers{}

	db := pdbcon.Connecting()
	defer db.Close()

	due := now.BeginningOfMonth()

	if err := db.Table("orders").Select("orders.*, users.first_name, users.last_name").Joins("inner join users on users.id = orders.user_id").Where("orders.user_id = users.id and orders.id LIKE ? and orders.status = 1 and orders.is_active =  ?", &ID, 1).Order("orders.id DESC").Scan(&orders).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetOrdersByID", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	for i, order := range orders {
		var count int
		if err := db.Model(&paymentDetails).Where("order_id = ?", &order.ID).Find(&paymentDetails).Count(&count).GetErrors(); len(err) > 0 {
			fmt.Println("Error get paymentDetails => " + err[0].Error())
		}
		if count < order.TotalInstallment && order.OrderDate.AddDate(0, count, 0).Before(due) {
			ordersReturn = append(ordersReturn, orders[i])
		}
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetOrdersByID", len(ordersReturn))
	return c.JSON(http.StatusOK, &out)
}

func GetOverDueOrdersByID(c echo.Context) error {
	start, err := strconv.Atoi(c.QueryParam("start"))

	if err != nil {
		out := pdbresponse.SetResponse(false, "Error GetRefundOrdersByID", "Error GetRefundOrdersByID")
		return c.JSON(http.StatusOK, &out)
	}

	ID := c.QueryParam("id")
	orders := []OrdersJoinUsers{}
	paymentDetails := []pdbpayment.PaymentDetail{}
	ordersReturn := []OrdersJoinUsers{}
	ID = "%" + ID + "%"

	db := pdbcon.Connecting()
	defer db.Close()

	due := now.BeginningOfMonth()

	if err := db.Table("orders").Select("orders.*, users.first_name, users.last_name").Joins("inner join users on users.id = orders.user_id").Where("orders.user_id = users.id and orders.id LIKE ? and orders.status = 1 and orders.is_active =  ?", &ID, 1).Order("orders.id DESC").Offset(start).Limit(5).Scan(&orders).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetOrdersByID", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	for i, order := range orders {

		var count int
		if err := db.Model(&paymentDetails).Where("order_id = ?", &order.ID).Find(&paymentDetails).Count(&count).GetErrors(); len(err) > 0 {
			fmt.Println("Error get paymentDetails => " + err[0].Error())
		}
		if count < order.TotalInstallment && order.OrderDate.AddDate(0, count, 0).Before(due) {

			if err := db.Where("order_id = ?", fmt.Sprint(order.ID)).Find(&orders[i].Products).GetErrors(); len(err) > 0 {
				out := pdbresponse.SetResponse(false, "Error GetOverDueOrdersByID", err)
				return c.JSON(http.StatusInternalServerError, &out)
			}

			if err := db.Model(&orders[i].PaymentDetails).Preload("DuitkuDetail").Where("order_id = ?", fmt.Sprint(order.ID)).Find(&orders[i].PaymentDetails).GetErrors(); len(err) > 0 {
				out := pdbresponse.SetResponse(false, "Error GetOverDueOrdersByID", err)
				return c.JSON(http.StatusInternalServerError, &out)
			}

			ordersReturn = append(ordersReturn, orders[i])
		}

	}

	out := pdbresponse.SetResponse(true, "Succesfully GetOrdersByID", &ordersReturn)
	return c.JSON(http.StatusOK, &out)
}

func GetHistoryOrdersByIDCount(c echo.Context) error {
	ID := c.QueryParam("id")
	ID = "%" + ID + "%"

	var count int

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Table("orders").Select("orders.*, users.first_name, users.last_name").Joins("inner join users on users.id = orders.user_id").Where("orders.user_id = users.id and orders.id LIKE ? and orders.status in (2,3) and orders.is_refund = 0 and orders.is_active =  ?", &ID, 1).Order("orders.id DESC").Count(&count).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetHistoryOrdersByIDCount", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetHistoryOrdersByIDCount", &count)
	return c.JSON(http.StatusOK, &out)
}

func GetHistoryOrdersByID(c echo.Context) error {
	start, err := strconv.Atoi(c.QueryParam("start"))

	if err != nil {
		out := pdbresponse.SetResponse(false, "Error GetHistoryOrdersByID", "Error GetHistoryOrdersByID")
		return c.JSON(http.StatusOK, &out)
	}

	ID := c.QueryParam("id")
	orders := []OrdersJoinUsers{}
	ID = "%" + ID + "%"

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Table("orders").Select("orders.*, users.first_name, users.last_name").Joins("inner join users on users.id = orders.user_id").Where("orders.user_id = users.id and orders.id LIKE ? and orders.status in (2,3) and orders.is_refund = 0 and orders.is_active =  ?", &ID, 1).Order("orders.id DESC").Offset(start).Limit(5).Scan(&orders).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetHistoryOrdersByID", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	for i, order := range orders {

		if err := db.Where("order_id = ?", fmt.Sprint(order.ID)).Find(&orders[i].Products).GetErrors(); len(err) > 0 {
			out := pdbresponse.SetResponse(false, "Error GetHistoryOrdersByID", err)
			return c.JSON(http.StatusInternalServerError, &out)
		}

		if err := db.Model(&orders[i].PaymentDetails).Preload("DuitkuDetail").Where("order_id = ?", fmt.Sprint(order.ID)).Find(&orders[i].PaymentDetails).GetErrors(); len(err) > 0 {
			out := pdbresponse.SetResponse(false, "Error GetHistoryOrdersByID", err)
			return c.JSON(http.StatusInternalServerError, &out)
		}

	}

	out := pdbresponse.SetResponse(true, "Succesfully GetHistoryOrdersByID", &orders)
	return c.JSON(http.StatusOK, &out)
}

type OrdersJoinUsers struct {
	gorm.Model
	TotalInstallment    int                        `json:"totalInstallment"`
	TotalPrice          decimal.Decimal            `json:"totalPrice" sql:"type:decimal(20,8);"`
	Status              int                        `json:"status"`
	IsRefund            bool                       `json:"isRefund"`
	OrderDate           time.Time                  `json:"orderDate"`
	CanceledDate        *time.Time                 `json:"canceledDate"`
	CompletedDate       *time.Time                 `json:"completedDate"`
	UserID              int                        `json:"userId"`
	HaveCancelDeadline  bool                       `json:"haveCancelDeadline"`
	IsActive            bool                       `json:"isActive"`
	DeadlineCancelMonth int                        `json:"deadlineCancelMonth"`
	LebaranMonth        int                        `json:"lebaranMonth"`
	FirstName           string                     `json:"firstName"`
	LastName            string                     `json:"lastName"`
	Products            []pdbproduct.Product       `json:"products"`
	PaymentDetails      []pdbpayment.PaymentDetail `json:"paymentDetails"`
}
