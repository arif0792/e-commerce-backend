package pdborder

import (
	"ALKA-backend/db/pdbcon"
	"ALKA-backend/pdbresponse"
	"ALKA-backend/pdbstruct/pdborder"
	"net/http"

	"github.com/labstack/echo"
)

func CancelOrder(c echo.Context) error {
	orderID := c.QueryParam("orderId")
	order := new(pdborder.Order)

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Model(&order).Where("id = ?", orderID).Find(&order).Update("status", 3).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error CancelOrder", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Success CancelOrder", &order)
	return c.JSON(http.StatusOK, &out)

}
