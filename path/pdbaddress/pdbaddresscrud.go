package pdbaddress

import (
	"ALKA-backend/db/pdbcon"
	"ALKA-backend/pdbresponse"
	"ALKA-backend/pdbstruct/pdbaddress"
	"fmt"
	"net/http"

	"github.com/labstack/echo"
)

//SaveAddress save
func SaveOrUpdateAddress(c echo.Context) error {
	addr := new(pdbaddress.Address)

	if err := c.Bind(addr); err != nil {
		fmt.Println("Error Data Binding")
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	if addr.ID == 0 {
		db.NewRecord(&addr)

		if err := db.Create(&addr).Error; err != nil {
			out := pdbresponse.SetResponse(false, "Error creating address", err.Error())
			return c.JSON(http.StatusInternalServerError, &out)
		}
	} else {
		if err := db.Save(&addr).Error; err != nil {
			out := pdbresponse.SetResponse(false, "Error update address", err.Error())
			return c.JSON(http.StatusInternalServerError, &out)
		}
	}

	out := pdbresponse.SetResponse(true, "Succesfully create/update address", &addr)
	return c.JSON(http.StatusOK, &out)
}

//DeleteAddressByAddressID deleteAddress
func DeleteAddressByAddressID(c echo.Context) error {
	addrID := c.QueryParam("addressID")
	addr := new(pdbaddress.Address)

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Where("id = ?", &addrID).Find(&addr).Delete(&addr).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error DeleteByAddrID", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully delete address by id", &addr)
	return c.JSON(http.StatusOK, &out)

}

//GetAddressesByUserID getAddrsByID
func GetAddressesByUserID(c echo.Context) error {
	userID := c.QueryParam("userId")
	addrs := []pdbaddress.Address{}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Where("user_id = ?", &userID).Find(&addrs).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetAddressesByUserId", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully get addresses by userid", &addrs)
	return c.JSON(http.StatusOK, &out)
}
