package pdbduitku

import (
	"ALKA-backend/assets/keys"
	"ALKA-backend/db/pdbcon"
	"ALKA-backend/pdbresponse"
	"ALKA-backend/pdbstruct/pdbduitkuDetail"
	"ALKA-backend/pdbstruct/pdborder"
	"ALKA-backend/pdbstruct/pdbpayment"
	"ALKA-backend/pdbstruct/pdbuser"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	fcm "github.com/NaySoftware/go-fcm"
	"github.com/jasonlvhit/gocron"
	"github.com/labstack/echo"
	"github.com/parnurzeal/gorequest"
)

func InitDuitkuPayment(c echo.Context) error {
	var url string
	var merchantKey string
	var merchantCode string

	if keys.Env == "DEV" {
		url = keys.Duitku_url
		merchantKey = keys.Duitku_merchantKey
		merchantCode = keys.Duitku_merchantCode
	} else {
		url = keys.Duitku_url_prod
		merchantKey = keys.Duitku_merchantKey_prod
		merchantCode = keys.Duitku_merchantCode_prod
	}

	callbackUrl := keys.Duitku_callbackUrl
	returnUrl := keys.Duitku_returnUrl

	paymentAmount, err := strconv.Atoi(c.QueryParam("paymentAmount"))
	paymentMethod := c.QueryParam("paymentMethod")
	merchantOrderId := c.QueryParam("merchantOrderId")
	productDetail := c.QueryParam("productDetail")
	h := md5.New()
	h.Write([]byte(merchantCode + merchantOrderId + c.QueryParam("paymentAmount") + merchantKey))
	signature := hex.EncodeToString(h.Sum(nil))
	email := c.QueryParam("email")

	requestDuitkuInquiry := RequestDuitkuInquiry{}
	responseDuitkuInquiry := ResponseDuitkuInquiry{}
	requestDuitkuInquiry.MerchantCode = merchantCode
	requestDuitkuInquiry.PaymentAmount = paymentAmount
	requestDuitkuInquiry.PaymentMethod = paymentMethod
	requestDuitkuInquiry.MerchantOrderId = merchantOrderId
	requestDuitkuInquiry.ProductDetail = productDetail
	requestDuitkuInquiry.CallbackUrl = callbackUrl
	requestDuitkuInquiry.ReturnUrl = returnUrl
	requestDuitkuInquiry.Signature = signature
	requestDuitkuInquiry.Email = email

	if err != nil {
		out := pdbresponse.SetResponse(false, "Error initUangkuPayment", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	fmt.Println(requestDuitkuInquiry)
	resp, _, errs := gorequest.New().Post(url).
		Set("Content-Type", "application/json").
		Send(requestDuitkuInquiry).EndStruct(&responseDuitkuInquiry)

	fmt.Println(resp)

	if len(errs) > 0 {
		out := pdbresponse.SetResponse(false, "Error initUangkuPayment", errs)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	if requestDuitkuInquiry.PaymentMethod == "A1" {
		s := gocron.NewScheduler()
		s.Every(24).Hours().Do(VAScheduler, s, responseDuitkuInquiry.Reference)
		s.Start()
	}

	out := pdbresponse.SetResponse(true, "Succesfully initUangkuPayment", &responseDuitkuInquiry)
	return c.JSON(http.StatusOK, &out)

}

func SaveOrUpdateCallBack(c echo.Context) error {
	var merchantKey string
	if keys.Env == "DEV" {
		//dev
		merchantKey = keys.Duitku_merchantKey
	} else {
		//prod
		merchantKey = keys.Duitku_merchantKey_prod
	}

	duitkuCallback := CallBackDuitku{}
	duitkuDetails := []pdbduitkuDetail.DuitkuDetail{}
	order := new(pdborder.Order)

	if err := c.Bind(&duitkuCallback); err != nil {
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		fmt.Println("Error => " + err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	h := md5.New()
	h.Write([]byte(duitkuCallback.MerchantCode + duitkuCallback.MerchantOrderId + duitkuCallback.Amount + merchantKey))
	signGenerate := hex.EncodeToString(h.Sum(nil))
	if signValid := duitkuCallback.Signature != signGenerate; !signValid {
		out := pdbresponse.SetResponse(false, "Error Signature callback", "invalid signature mismatch")
		fmt.Println("invalid signature mismatch")
		return c.JSON(http.StatusInternalServerError, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Model(&duitkuDetails).Where("ref_id = ?", &duitkuCallback.Reference).Find(&duitkuDetails).Update("result_code", &duitkuCallback.ResultCode).Error; err != nil {
		out := pdbresponse.SetResponse(false, "Error SaveOrUpdateCallBack", err)
		fmt.Println("Error => " + err.Error())
		return c.JSON(http.StatusInternalServerError, &out)
	}

	if duitkuCallback.ResultCode == "00" {

		if err := db.Model(&order).Where("id = ?", &duitkuDetails[0].OrderID).Find(&order).Update("status", 1).Error; err != nil {
			out := pdbresponse.SetResponse(false, "Error SaveOrUpdateCallBack", err)
			fmt.Println("Error => " + err.Error())
			return c.JSON(http.StatusInternalServerError, &out)
		}

	} else {

		if err := db.Model(&order).Where("id = ?", &duitkuDetails[0].OrderID).Find(&order).Update("status", 0).Error; err != nil {
			out := pdbresponse.SetResponse(false, "Error SaveOrUpdateCallBack", err)
			fmt.Println("Error => " + err.Error())
			return c.JSON(http.StatusInternalServerError, &out)
		}

	}

	//fcm
	order.PaymentDetails = nil
	order.Products = nil
	jsonMsg, _ := json.Marshal(order)
	data := map[string]interface{}{
		"order": string(jsonMsg),
	}

	// var ids []string

	user := pdbuser.User{}
	if err := db.Table("users").Where("id = ?", &order.UserID).Find(&user).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetOrdersByUserId", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	// ids = []string{
	// 	user.FcmToken,
	// }

	f := fcm.NewFcmClient(keys.FcmServerKey)
	// f.NewFcmRegIdsMsg(ids, data)
	//change using topic / user
	f.NewFcmMsgTo("/topics/userId"+fmt.Sprint(user.ID), data)

	status, err := f.Send()

	if err == nil {
		status.PrintResults()
	} else {
		out := pdbresponse.SetResponse(false, "Error FCM", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	//duitkudetails
	for _, duitkuDetail := range duitkuDetails {
		jsonMsg2, _ := json.Marshal(duitkuDetail)
		data1 := map[string]interface{}{
			"duitkuDetail": string(jsonMsg2),
		}
		g := fcm.NewFcmClient(keys.FcmServerKey)
		g.NewFcmMsgTo("/topics/userId"+fmt.Sprint(user.ID), data1)
		status1, err1 := g.Send()
		if err1 == nil {
			status1.PrintResults()
		} else {
			out := pdbresponse.SetResponse(false, "Error FCM", err)
			return c.JSON(http.StatusInternalServerError, &out)
		}
	}
	//
	//endfcm

	if duitkuCallback.ResultCode == "00" {

		resp := new(pdbresponse.OutStruct)
		msg := new(pdbuser.Message)
		msg.From = "Admin"
		msg.UserID = order.UserID
		msg.Data = "Pembayaran anda dengan nomor Order #" + fmt.Sprint(order.ID) + " telah berhasil. Terima kasih."

		gorequest.New().Post(keys.OwnAddress+"/panen/v1/account/message").
			Set("Content-Type", "application/json").
			Send(msg).EndStruct(&resp)

		if !resp.Success {
			out := pdbresponse.SetResponse(false, "Error Send Msg", resp)
			return c.JSON(http.StatusInternalServerError, &out)
		}

	}

	out := `SUCCESS`
	return c.JSON(http.StatusOK, &out)
}

func VAScheduler(s *gocron.Scheduler, refID string) {

	duitkuDetails := []pdbduitkuDetail.DuitkuDetail{}
	paymentDetails := []pdbpayment.PaymentDetail{}
	order := new(pdborder.Order)

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Model(&duitkuDetails).Where("ref_id = ?", &refID).Find(&duitkuDetails).Error; err != nil {
		fmt.Println("Error => " + err.Error())
	}

	if duitkuDetails[0].ResultCode == "01" {
		if err := db.Model(&duitkuDetails).Where("ref_id = ?", &refID).Find(&duitkuDetails).Delete(&duitkuDetails).Error; err != nil {
			fmt.Println("Error => " + err.Error())
		}
		if err := db.Model(&paymentDetails).Where("order_id = ?", &duitkuDetails[0].OrderID).Find(&paymentDetails).Delete(&paymentDetails).Error; err != nil {
			fmt.Println("Error => " + err.Error())
		}
		if err := db.Model(&order).Where("id = ?", &duitkuDetails[0].OrderID).Find(&order).Update("status", 1).Error; err != nil {
			fmt.Println("Error => " + err.Error())
		}
		if err := db.Model(&duitkuDetails).Where("order_id = ?", &order.ID).Find(&duitkuDetails).Error; err != nil {
			fmt.Println("Error => " + err.Error())
		}
		fmt.Println("test", duitkuDetails, len(duitkuDetails))
		if len(duitkuDetails) == 0 {
			if err := db.Model(&order).Where("id = ?", &order.ID).Find(&order).Delete(&order).Error; err != nil {
				fmt.Println("Error => " + err.Error())
			}
		}
	}

	s.Clear()
}

type RequestDuitkuInquiry struct {
	PaymentAmount   int    `json:"paymentAmount"`
	MerchantOrderId string `json:"merchantOrderId"`
	ProductDetail   string `json:"productDetail"`
	Email           string `json:"email"`
	PaymentMethod   string `json:"paymentMethod"`
	Signature       string `json:"signature"`
	MerchantCode    string `json:"merchantCode"`
	ReturnUrl       string `json:"returnUrl"`
	CallbackUrl     string `json:"callbackUrl"`
}

type ResponseDuitkuInquiry struct {
	MerchantCode  string `json:"merchantCode"`
	Reference     string `json:"reference"`
	PaymentUrl    string `json:"paymentUrl"`
	VaNumber      string `json:"vaNumber"`
	StatusCode    string `json:"statusCode"`
	StatusMessage string `json:"statusMessage"`
}

type CallBackDuitku struct {
	MerchantCode    string `form:"merchantCode"`
	Amount          string `form:"amount"`
	MerchantOrderId string `form:"merchantOrderId"`
	ProductDetail   string `form:"productDetail"`
	PaymentMethod   string `form:"paymentCode"`
	Signature       string `form:"signature"`
	ResultCode      string `form:"resultCode"`
	MerchantUserId  string `form:"merchantUserId"`
	Reference       string `form:"reference"`
}
