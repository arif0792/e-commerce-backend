package pdbinquiry

import (
	"ALKA-backend/db/pdbcon"
	"ALKA-backend/pdbresponse"
	"ALKA-backend/pdbstruct/pdbinquiry"
	"fmt"
	"net/http"

	"github.com/labstack/echo"
)

func GetInquiries(c echo.Context) error {
	inquiries := []pdbinquiry.Inquiry{}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Find(&inquiries).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetInquiries", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetInquiries", &inquiries)
	return c.JSON(http.StatusOK, &out)

}

func PostInquiry(c echo.Context) error {
	inquiry := pdbinquiry.Inquiry{}

	if err := c.Bind(&inquiry); err != nil {
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		fmt.Println("Error => " + err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	db.NewRecord(&inquiry)

	if err := db.Create(&inquiry).Error; err != nil {
		out := pdbresponse.SetResponse(false, "Error creating PostInquiry", err.Error())
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully PostInquiry", &inquiry)
	return c.JSON(http.StatusOK, &out)

}
