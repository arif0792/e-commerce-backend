package pdbaccount

import (
	"ALKA-backend/db/pdbcon"
	"ALKA-backend/pdbresponse"
	"ALKA-backend/pdbstruct/pdbuser"
	"fmt"
	"net/http"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

func Login(c echo.Context) error {
	acctUserOnly := new(pdbuser.User)

	if err := c.Bind(&acctUserOnly); err != nil {
		fmt.Println("Error Data Binding")
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	/* db.Model(&acct).Preload("Addresses").Where("user_name = ? AND password = ? ", acct.UserName, acct.Password).Find(&acct)
	 */
	if err := db.Model(&acctUserOnly).Preload("AdminPrivileges").Preload("Reads").Preload("Addresses").Preload("Orders", func(db *gorm.DB) *gorm.DB {
		return db.Where("orders.is_active = ? ", 1).Order("orders.id DESC")
	}).Preload("Orders.Products").Preload("Orders.PaymentDetails", func(db *gorm.DB) *gorm.DB {
		return db.Order("payment_details.id DESC")
	}).Preload("Orders.PaymentDetails.DuitkuDetail", func(db *gorm.DB) *gorm.DB {
		return db.Order("duitku_details.id DESC")
	}).Where("email = ?", &acctUserOnly.Email).Find(&acctUserOnly).RecordNotFound(); err {
		fmt.Println("Record Not Found acct")
		out := pdbresponse.SetResponse(false, "Error Login", "Not Found")
		return c.JSON(http.StatusOK, &out)
	}

	if err := db.Where("user_id in (?) AND created_at >= ?", []string{fmt.Sprint(acctUserOnly.ID), "0"}, &acctUserOnly.CreatedAt).Order("id DESC").Find(&acctUserOnly.Messages).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetMessagesByUserID", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully login", &acctUserOnly)
	return c.JSON(http.StatusOK, &out)

}

func UsernameCheck(c echo.Context) error {
	acct := new(pdbuser.User)

	db := pdbcon.Connecting()
	defer db.Close()

	if err := c.Bind(&acct); err != nil {
		fmt.Println("Error Data Binding")
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	if err := db.Where("user_name = ?", acct.UserName).Find(&acct).RecordNotFound(); err {
		out := pdbresponse.SetResponse(true, "Succesfully Username check", "Username available")
		return c.JSON(http.StatusOK, &out)
	} else {
		out := pdbresponse.SetResponse(false, "Failed Username check", "Username unavailable")
		return c.JSON(http.StatusOK, &out)
	}

}

func PasswordCheck(c echo.Context) error {
	acct := new(pdbuser.User)

	db := pdbcon.Connecting()
	defer db.Close()

	if err := c.Bind(&acct); err != nil {
		fmt.Println("Error Data Binding")
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	if err := db.Where("id = ? and password = ?", acct.ID, acct.Password).Find(&acct).RecordNotFound(); err {
		out := pdbresponse.SetResponse(false, "Failed PasswordCheck", "Password wrong")
		return c.JSON(http.StatusOK, &out)
	} else {
		out := pdbresponse.SetResponse(true, "Succesfully PasswordCheck", "Password legit")
		return c.JSON(http.StatusOK, &out)
	}

}

func EmailCheck(c echo.Context) error {
	acct := new(pdbuser.User)

	db := pdbcon.Connecting()
	defer db.Close()

	if err := c.Bind(&acct); err != nil {
		fmt.Println("Error Data Binding")
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	if err := db.Where("email = ?", acct.Email).Find(&acct).RecordNotFound(); err {
		out := pdbresponse.SetResponse(true, "Succesfully Email check", "Email available")
		return c.JSON(http.StatusOK, &out)
	} else {
		out := pdbresponse.SetResponse(false, "Failed Email check", "Email unavailable")
		return c.JSON(http.StatusOK, &out)
	}

}

func PhoneNumberCheck(c echo.Context) error {
	acct := new(pdbuser.User)

	db := pdbcon.Connecting()
	defer db.Close()

	if err := c.Bind(&acct); err != nil {
		fmt.Println("Error Data Binding")
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	if err := db.Where("phone_number = ?", acct.PhoneNumber).Find(&acct).RecordNotFound(); err {
		out := pdbresponse.SetResponse(true, "Succesfully PhoneNumberCheck", "Email available")
		return c.JSON(http.StatusOK, &out)
	} else {
		out := pdbresponse.SetResponse(false, "Failed PhoneNumberCheck", "Email unavailable")
		return c.JSON(http.StatusOK, &out)
	}

}
