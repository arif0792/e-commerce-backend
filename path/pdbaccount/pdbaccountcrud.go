package pdbaccount

import (
	"ALKA-backend/assets/keys"
	"ALKA-backend/db/pdbcon"
	"ALKA-backend/pdbresponse"
	"ALKA-backend/pdbstruct/pdbuser"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/auth"
	fcm "github.com/NaySoftware/go-fcm"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
	"google.golang.org/api/option"
)

func SaveOrUpdateAccount(c echo.Context) error {

	//firebase
	opt := option.WithCredentialsFile("assets/servicekeys.json")
	firebaseApp, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		out := pdbresponse.SetResponse(false, "error initializing firebaseapp", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	// Get an auth client from the firebase.App
	authClient, err := firebaseApp.Auth(context.Background())
	if err != nil {
		out := pdbresponse.SetResponse(false, "error getting Auth client", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	acct := new(pdbuser.User)

	if err := c.Bind(acct); err != nil {
		fmt.Println("Error Data Binding")
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	if acct.ID == 0 {

		//firebase bind
		params := (&auth.UserToCreate{}).
			Email(acct.Email).
			EmailVerified(true).
			PhoneNumber(acct.PhoneNumber).
			Password(acct.Password).
			DisplayName(acct.FirstName + " " + acct.LastName).
			Disabled(false)
		u, err := authClient.CreateUser(context.Background(), params)
		if err != nil {
			out := pdbresponse.SetResponse(false, "error creating user firebase", err.Error())
			return c.JSON(http.StatusInternalServerError, &out)
		}

		acct.FirebaseUID = u.UserInfo.UID

		db.NewRecord(&acct)

		if err := db.Create(&acct).Error; err != nil {
			out := pdbresponse.SetResponse(false, "Error creating new account", err.Error())
			return c.JSON(http.StatusInternalServerError, &out)
		}

	} else {
		if err := db.Save(&acct).Error; err != nil {
			out := pdbresponse.SetResponse(false, "Error update user", err.Error)
			return c.JSON(http.StatusInternalServerError, &out)
		}

		//firebase bind
		params := (&auth.UserToUpdate{}).
			Email(acct.Email).
			EmailVerified(true).
			PhoneNumber(acct.PhoneNumber).
			DisplayName(acct.FirstName + " " + acct.LastName).
			Disabled(false)
		_, err := authClient.UpdateUser(context.Background(), acct.FirebaseUID, params)
		if err != nil {
			out := pdbresponse.SetResponse(false, "error update user firebase", err.Error())
			return c.JSON(http.StatusInternalServerError, &out)
		}
	}

	out := pdbresponse.SetResponse(true, "Succesfully create/update account", &acct)
	return c.JSON(http.StatusOK, &out)

}

func ChangePasswordAccount(c echo.Context) error {

	//firebase
	opt := option.WithCredentialsFile("assets/servicekeys.json")
	firebaseApp, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		out := pdbresponse.SetResponse(false, "error initializing firebaseapp", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	// Get an auth client from the firebase.App
	authClient, err := firebaseApp.Auth(context.Background())
	if err != nil {
		out := pdbresponse.SetResponse(false, "error getting Auth client", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	acct := new(pdbuser.User)

	if err := c.Bind(acct); err != nil {
		fmt.Println("Error Data Binding")
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Table("users").Where("id = ?", &acct.ID).Update("password", &acct.Password).Error; err != nil {
		out := pdbresponse.SetResponse(false, "Error update password", err.Error)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	//firebase bind
	params := (&auth.UserToUpdate{}).
		Password(acct.Password)
	_, errs := authClient.UpdateUser(context.Background(), acct.FirebaseUID, params)
	if errs != nil {
		out := pdbresponse.SetResponse(false, "error update password firebase", err.Error())
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully update password", &acct)
	return c.JSON(http.StatusOK, &out)

}

func UpdateFCMToken(c echo.Context) error {
	interfaceForUpdate := new(struct {
		UserId    string `json:"userId"`
		UserToken string `json:"userToken"`
	})

	if err := c.Bind(interfaceForUpdate); err != nil {
		fmt.Println("Error Data Binding")
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Table("users").Where("id = ?", &interfaceForUpdate.UserId).Update("fcm_token", &interfaceForUpdate.UserToken).Error; err != nil {
		out := pdbresponse.SetResponse(false, "Error UpdateFirebaseToken", err.Error)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully UpdateFirebaseToken", "success update token")
	return c.JSON(http.StatusOK, &out)
}

func SaveOrUpdateMessage(c echo.Context) error {
	msg := new(pdbuser.Message)

	if err := c.Bind(msg); err != nil {
		fmt.Println("Error Data Binding")
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	if msg.ID == 0 {
		db.NewRecord(&msg)

		if err := db.Create(&msg).Error; err != nil {
			out := pdbresponse.SetResponse(false, "Error creating Message", err.Error())
			return c.JSON(http.StatusInternalServerError, &out)
		}
	} else {
		if err := db.Save(&msg).Error; err != nil {
			out := pdbresponse.SetResponse(false, "Error update Message", err.Error())
			return c.JSON(http.StatusInternalServerError, &out)
		}
	}

	//fcm
	jsonMsg, _ := json.Marshal(msg)
	data := map[string]interface{}{
		"message": string(jsonMsg),
	}

	var NP fcm.NotificationPayload
	NP.Body = msg.Data
	NP.ClickAction = "FCM_PLUGIN_ACTIVITY"

	// var ids []string

	f := fcm.NewFcmClient(keys.FcmServerKey)

	if msg.UserID == 0 {

		f.NewFcmMsgTo("/topics/broadcast", data)

	} else {

		user := pdbuser.User{}
		if err := db.Table("users").Where("id = ?", &msg.UserID).Find(&user).GetErrors(); len(err) > 0 {
			out := pdbresponse.SetResponse(false, "Error GetOrdersByUserId", err)
			return c.JSON(http.StatusInternalServerError, &out)
		}

		// ids = []string{
		// 	user.FcmToken,
		// }

		// f.NewFcmRegIdsMsg(ids, data)
		//change using topic / user
		f.NewFcmMsgTo("/topics/userId"+fmt.Sprint(user.ID), data)

	}
	f.SetNotificationPayload(&NP)
	status, err := f.Send()
	if err == nil {
		status.PrintResults()
	} else {
		out := pdbresponse.SetResponse(false, "Error FCM", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}
	//end fcm

	out := pdbresponse.SetResponse(true, "Succesfully create/update Message", &msg)
	return c.JSON(http.StatusOK, &out)
}

func GetMessagesByUserID(c echo.Context) error {
	userID := c.QueryParam("userId")
	acct := new(pdbuser.User)
	msges := []pdbuser.Message{}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Model(&acct).Where("id = ? ", &userID).Find(&acct).RecordNotFound(); err {
		fmt.Println("Acct Record Not Found acct")
		out := pdbresponse.SetResponse(false, "Error query acct", "Not Found")
		return c.JSON(http.StatusInternalServerError, &out)
	}

	if err := db.Where("user_id in (?) AND created_at >= ?", []string{userID, "0"}, &acct.CreatedAt).Order("id DESC").Find(&msges).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetMessagesByUserID", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetMessagesByUserID", &msges)
	return c.JSON(http.StatusOK, &out)
}

func SaveOrUpdateRead(c echo.Context) error {
	read := new(pdbuser.Read)

	if err := c.Bind(read); err != nil {
		fmt.Println("Error Data Binding")
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	if read.ID == 0 {
		db.NewRecord(&read)

		if err := db.Create(&read).Error; err != nil {
			out := pdbresponse.SetResponse(false, "Error creating Read", err.Error())
			return c.JSON(http.StatusInternalServerError, &out)
		}
	} else {
		if err := db.Save(&read).Error; err != nil {
			out := pdbresponse.SetResponse(false, "Error update Read", err.Error())
			return c.JSON(http.StatusInternalServerError, &out)
		}
	}

	out := pdbresponse.SetResponse(true, "Succesfully create/update Read", &read)
	return c.JSON(http.StatusOK, &out)
}

func GetReadsByUserID(c echo.Context) error {
	userID := c.QueryParam("userId")
	reads := []pdbuser.Read{}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Where("user_id = ?", &userID).Find(&reads).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetReadsByUserID", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetReadsByUserID", &reads)
	return c.JSON(http.StatusOK, &out)
}

func GetUsersCountByName(c echo.Context) error {
	name := c.QueryParam("name")
	users := []pdbuser.User{}
	name = "%" + name + "%"

	db := pdbcon.Connecting()
	defer db.Close()

	if name != "" {
		sql := "select * from users where first_name LIKE ? union select * from users where last_name LIKE ?"
		if err := db.Raw(sql, &name, &name).Scan(&users).GetErrors(); len(err) > 0 {
			out := pdbresponse.SetResponse(false, "Error GetUsers", err)
			return c.JSON(http.StatusInternalServerError, &out)
		}

	} else {
		sql := "select * from users"
		if err := db.Raw(sql).Scan(&users).GetErrors(); len(err) > 0 {
			out := pdbresponse.SetResponse(false, "Error GetUsers", err)
			return c.JSON(http.StatusInternalServerError, &out)
		}
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetUsers Count", len(users))
	return c.JSON(http.StatusOK, &out)
}

func checkCount(rows *sql.Rows) (count int) {
	for rows.Next() {
		err := rows.Scan(rows, &count)
		checkErr(err)
	}
	return count
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func GetUserByID(c echo.Context) error {
	acct := new(pdbuser.User)
	id, err := strconv.Atoi(c.QueryParam("id"))

	if err != nil {
		out := pdbresponse.SetResponse(false, "Error GetUserByID", "Error GetUserByID")
		return c.JSON(http.StatusOK, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Model(&acct).Preload("AdminPrivileges").Preload("Reads").Preload("Addresses").Preload("Orders", func(db *gorm.DB) *gorm.DB {
		return db.Where("orders.is_active = 1").Order("orders.id DESC")
	}).Preload("Orders.Products").Preload("Orders.PaymentDetails", func(db *gorm.DB) *gorm.DB {
		return db.Order("payment_details.id DESC")
	}).Preload("Orders.PaymentDetails.DuitkuDetail", func(db *gorm.DB) *gorm.DB {
		return db.Order("duitku_details.id DESC")
	}).Where("id = ?", &id).Find(&acct).RecordNotFound(); err {
		fmt.Println("Record Not Found acct")
		out := pdbresponse.SetResponse(false, "GetUserByID", "Not Found")
		return c.JSON(http.StatusOK, &out)
	}

	if err := db.Where("user_id in (?) AND created_at >= ?", []string{fmt.Sprint(acct.ID), "0"}, &acct.CreatedAt).Find(&acct.Messages).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetMessagesByUserID", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetUserByID", &acct)
	return c.JSON(http.StatusOK, &out)
}

func GetUsersByName(c echo.Context) error {
	start, err := strconv.Atoi(c.QueryParam("start"))

	if err != nil {
		out := pdbresponse.SetResponse(false, "Error GetUsersByName", "Error GetUsersByName")
		return c.JSON(http.StatusOK, &out)
	}

	name := c.QueryParam("name")
	users := []pdbuser.User{}
	name = "%" + name + "%"

	db := pdbcon.Connecting()
	defer db.Close()

	if name != "" {
		if err := db.Model(&users).Preload("AdminPrivileges").Preload("Reads").Preload("Addresses").Preload("Orders", func(db *gorm.DB) *gorm.DB {
			return db.Where("orders.is_active = 1").Order("orders.id DESC")
		}).Preload("Orders.Products").Preload("Orders.PaymentDetails", func(db *gorm.DB) *gorm.DB {
			return db.Order("payment_details.id DESC")
		}).Preload("Orders.PaymentDetails.DuitkuDetail", func(db *gorm.DB) *gorm.DB {
			return db.Order("duitku_details.id DESC")
		}).Where("first_name LIKE ? OR last_name LIKE ? ", &name, &name).Offset(start).Limit(5).Find(&users).RecordNotFound(); err {
			fmt.Println("Record Not Found acct")
			out := pdbresponse.SetResponse(false, "Error GetUsersByName", "Error GetUsersByName")
			return c.JSON(http.StatusOK, &out)
		}
	} else {
		if err := db.Model(&users).Preload("AdminPrivileges").Preload("Reads").Preload("Addresses").Preload("Orders", func(db *gorm.DB) *gorm.DB {
			return db.Where("orders.is_active = 1").Order("orders.id DESC")
		}).Preload("Orders.Products").Preload("Orders.PaymentDetails", func(db *gorm.DB) *gorm.DB {
			return db.Order("payment_details.id DESC")
		}).Preload("Orders.PaymentDetails.DuitkuDetail", func(db *gorm.DB) *gorm.DB {
			return db.Order("duitku_details.id DESC")
		}).Offset(start).Limit(5).Find(&users).RecordNotFound(); err {
			fmt.Println("Record Not Found acct")
			out := pdbresponse.SetResponse(false, "Error GetUsersByName", "Error GetUsersByName")
			return c.JSON(http.StatusOK, &out)
		}
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetUsers", &users)
	return c.JSON(http.StatusOK, &out)
}
