package pdbproduct

import (
	"ALKA-backend/assets/keys"
	"ALKA-backend/db/pdbcon"
	"ALKA-backend/pdbresponse"
	"ALKA-backend/pdbstruct/pdborder"
	"ALKA-backend/pdbstruct/pdbproduct"
	"ALKA-backend/pdbstruct/pdbuser"
	decimal "ALKA-backend/shopspring-decimal"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	fcm "github.com/NaySoftware/go-fcm"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

func SaveOrUpdateProducts(c echo.Context) error {
	products := []pdbproduct.Product{}
	dbProducts := []pdbproduct.Product{}
	order := pdborder.Order{}

	if err := c.Bind(&products); err != nil {
		fmt.Println("Error Data Binding")
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	for _, product := range products {

		if product.ID == 0 {
			db.NewRecord(&product)

			if err := db.Create(&product).Error; err != nil {
				out := pdbresponse.SetResponse(false, "Error creating product", err.Error())
				return c.JSON(http.StatusInternalServerError, &out)
			}
		} else {
			if err := db.Save(&product).Error; err != nil {
				out := pdbresponse.SetResponse(false, "Error update product", err.Error())
				return c.JSON(http.StatusInternalServerError, &out)
			}
			if product.Status == 2 {

				if err := db.Model(&dbProducts).Where("order_id = ?", &product.OrderID).Find(&dbProducts).Error; err != nil {
					out := pdbresponse.SetResponse(false, "Error update status order", err.Error)
					return c.JSON(http.StatusInternalServerError, &out)
				}

				allCompleted := true
				for _, prod := range dbProducts {

					fmt.Println(prod.Status)
					if prod.Status != 2 {
						allCompleted = false
					}
				}

				if allCompleted == true {
					if err := db.Model(&order).Where("id = ?", &product.OrderID).Update("status", 2).Update("completed_date", time.Now()).Error; err != nil {
						out := pdbresponse.SetResponse(false, "Error update status order", err.Error)
						return c.JSON(http.StatusInternalServerError, &out)
					}
				}
			}
		}
	}

	//admin hanya kirim 1 per request jadi pake array ke 0
	if len(products) == 1 {

		//fcm
		jsonMsg, _ := json.Marshal(products[0])
		data := map[string]interface{}{
			"product": string(jsonMsg),
		}

		// var ids []string

		user := pdbuser.User{}

		if err := db.Model(&order).Where("id = ?", &products[0].OrderID).Find(&order).Error; err != nil {
			out := pdbresponse.SetResponse(false, "Error update status order", err.Error)
			return c.JSON(http.StatusInternalServerError, &out)
		}

		if err := db.Table("users").Where("id = ?", &order.UserID).Find(&user).GetErrors(); len(err) > 0 {
			out := pdbresponse.SetResponse(false, "Error GetOrdersByUserId", err)
			return c.JSON(http.StatusInternalServerError, &out)
		}

		// ids = []string{
		// 	user.FcmToken,
		// }

		f := fcm.NewFcmClient(keys.FcmServerKey)
		// f.NewFcmRegIdsMsg(ids, data)
		//change using topic / user
		f.NewFcmMsgTo("/topics/userId"+fmt.Sprint(user.ID), data)

		status, err := f.Send()
		if err == nil {
			status.PrintResults()
		} else {
			out := pdbresponse.SetResponse(false, "Error FCM", err)
			return c.JSON(http.StatusInternalServerError, &out)
		}
		//endfcm

	}

	fmt.Println(&products)
	out := pdbresponse.SetResponse(true, "Succesfully create/update products", &products)
	return c.JSON(http.StatusOK, &out)
}

func GetProductsByOrderID(c echo.Context) error {
	orderID := c.QueryParam("orderId")
	products := []pdbproduct.Product{}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Model(&products).Where("order_id = ?", &orderID).Find(&products).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetOrdersByUserId", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully get orders by userid", &products)
	return c.JSON(http.StatusOK, &out)
}

func GetProductsByKelurahanCount(c echo.Context) error {
	name := c.QueryParam("name")
	name = "%" + name + "%"
	var count int

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Table("products").Select("products.*, addresses.kelurahan").Joins("inner join orders on orders.id = products.order_id").Joins("left join addresses on addresses.id = products.alamat_id").Where("products.order_id = orders.id and orders.status = 1 and products.item_option = 'DELIVERY' and products.status != 2 and products.product_id LIKE ? and orders.total_installment + 1 = (select COUNT(*) from payment_details pd where pd.order_id = orders.id and pd.deleted_at IS NULL)", &name).Order("addresses.kelurahan DESC").Count(&count).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetProductsByKelurahanCount", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetProductsByKelurahanCount", &count)
	return c.JSON(http.StatusOK, &out)
}

func GetProductsByKelurahan(c echo.Context) error {
	start, err := strconv.Atoi(c.QueryParam("start"))

	if err != nil {
		out := pdbresponse.SetResponse(false, "Error GetProductsByKelurahan", "Error GetProductsByKelurahan")
		return c.JSON(http.StatusInternalServerError, &out)
	}

	productJoinAddress := []ProductJoinAddress{}
	name := c.QueryParam("name")
	name = "%" + name + "%"

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Table("products").Select("products.*, addresses.address, addresses.postal_code, addresses.home_no, addresses.kelurahan, users.first_name, users.last_name, orders.user_id").Joins("inner join orders on orders.id = products.order_id").Joins("left join addresses on addresses.id = products.alamat_id").Joins("left join users on users.id = orders.user_id ").Where("products.order_id = orders.id and orders.status = 1 and products.item_option = 'DELIVERY' and products.status != 2 and products.product_id LIKE ? and orders.total_installment + 1 = (select COUNT(*) from payment_details pd where pd.order_id = orders.id and pd.deleted_at IS NULL)", &name).Order("addresses.kelurahan DESC").Offset(start).Limit(5).Scan(&productJoinAddress).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetProductsByKelurahan", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetProductsByKelurahan", &productJoinAddress)
	return c.JSON(http.StatusOK, &out)
}

func GetProductsByPotongCount(c echo.Context) error {
	name := c.QueryParam("name")
	name = "%" + name + "%"
	var count int

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Table("products").Select("products.*").Joins("inner join orders on orders.id = products.order_id").Where("products.order_id = orders.id and orders.status = 1 and products.item_option = 'POTONG' and products.status != 2 and products.product_id LIKE ? and orders.total_installment + 1 = (select COUNT(*) from payment_details pd where pd.order_id = orders.id and pd.deleted_at IS NULL)", &name).Order("products.product_id DESC").Count(&count).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetProductsByPotongCount", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetProductsByPotongCount", &count)
	return c.JSON(http.StatusOK, &out)
}

func GetProductsByPotong(c echo.Context) error {
	start, err := strconv.Atoi(c.QueryParam("start"))

	if err != nil {
		out := pdbresponse.SetResponse(false, "Error GetProductsByPotong", "Error GetProductsByPotong")
		return c.JSON(http.StatusInternalServerError, &out)
	}

	productJoinAddress := []ProductJoinAddress{}
	name := c.QueryParam("name")
	name = "%" + name + "%"

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Table("products").Select("products.*, users.first_name, users.last_name, orders.user_id").Joins("inner join orders on orders.id = products.order_id").Joins("left join users on users.id = orders.user_id ").Where("products.order_id = orders.id and orders.status = 1 and products.item_option = 'POTONG' and products.status != 2 and products.product_id LIKE ? and orders.total_installment + 1 = (select COUNT(*) from payment_details pd where pd.order_id = orders.id and pd.deleted_at IS NULL)", &name).Order("products.product_id DESC").Offset(start).Limit(5).Scan(&productJoinAddress).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetProductsByPotong", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetProductsByPotong", &productJoinAddress)
	return c.JSON(http.StatusOK, &out)
}

type ProductJoinAddress struct {
	gorm.Model
	ProductID       string          `json:"productId"`
	Status          int             `json:"status"`
	AlamatID        int             `json:"alamatId"`
	OrderID         uint            `json:"orderId"`
	ItemName        string          `json:"itemName"`
	ItemPrice       decimal.Decimal `json:"itemPrice" sql:"type:decimal(20,8);"`
	ItemAdminPrice  decimal.Decimal `json:"itemAdminPrice" sql:"type:decimal(20,8);"`
	ItemOptionPrice decimal.Decimal `json:"itemOptionPrice" sql:"type:decimal(20,8);"`
	ItemOption      string          `json:"itemOption"`
	Address         string          `json:"address"`
	PostalCode      int             `json:"postalCode"`
	HomeNo          string          `json:"homeNo"`
	Kelurahan       string          `json:"kelurahan"`
	FirstName       string          `json:"firstName"`
	LastName        string          `json:"lastName"`
	UserID          int             `json:"userId"`
}
