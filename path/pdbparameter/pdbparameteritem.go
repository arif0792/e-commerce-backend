package pdbparameter

import (
	"ALKA-backend/db/pdbcon"
	"ALKA-backend/pdbresponse"
	"ALKA-backend/pdbstruct/pdbitem"
	"fmt"
	"net/http"

	"github.com/labstack/echo"
)

func GetParameterItemsByCategory(c echo.Context) error {
	itemID := c.QueryParam("id")
	items := []pdbitem.Item{}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Find(&items).Where("item_category = ?", &itemID).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetParameterItemsByCategory", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetParameterItemsByCategory", &items)
	return c.JSON(http.StatusOK, &out)

}

func GetParameterItems(c echo.Context) error {
	items := []pdbitem.Item{}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Find(&items).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetParameterItems", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetParameterItems", &items)
	return c.JSON(http.StatusOK, &out)

}

func DeleteParameterItem(c echo.Context) error {
	itemID := c.QueryParam("id")
	item := pdbitem.Item{}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Where("id = ?", &itemID).Find(&item).Delete(&item).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error DeleteParameterItem", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully DeleteParameterItem", &item)
	return c.JSON(http.StatusOK, &out)

}

func PostParameterItems(c echo.Context) error {
	items := []pdbitem.Item{}

	if err := c.Bind(&items); err != nil {
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		fmt.Println("Error => " + err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	for _, item := range items {

		if item.ID == 0 {

			db.NewRecord(&item)

			if err := db.Create(&item).Error; err != nil {
				out := pdbresponse.SetResponse(false, "Error creating item", err.Error())
				return c.JSON(http.StatusInternalServerError, &out)
			}

		} else {

			if err := db.Save(&item).GetErrors(); len(err) > 0 {
				out := pdbresponse.SetResponse(false, "Error update item", err)
				return c.JSON(http.StatusInternalServerError, &out)
			}

		}

	}

	out := pdbresponse.SetResponse(true, "Succesfully PostParameterItems", &items)
	return c.JSON(http.StatusOK, &out)

}

func GetItemPictures(c echo.Context) error {
	q := c.Request().URL.Query() // Parse only once
	picIDs := q["id"]

	pics := []pdbitem.Picture{}

	db := pdbcon.Connecting()
	defer db.Close()

	fmt.Println(picIDs)
	if err := db.LogMode(true).Where("id in (?)", picIDs).Find(&pics).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetItemPicture", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetItemPicture", &pics)
	return c.JSON(http.StatusOK, &out)

}

func PostItemPictures(c echo.Context) error {
	pics := []pdbitem.Picture{}

	if err := c.Bind(&pics); err != nil {
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		fmt.Println("Error => " + err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	for i, pic := range pics {

		if pic.ID == 0 {

			db.NewRecord(&pic)

			if err := db.Create(&pic).Error; err != nil {
				out := pdbresponse.SetResponse(false, "Error creating pic", err.Error())
				return c.JSON(http.StatusInternalServerError, &out)
			}

			pics[i] = pic

		} else {

			if err := db.Save(&pic).GetErrors(); len(err) > 0 {
				out := pdbresponse.SetResponse(false, "Error update pic", err)
				return c.JSON(http.StatusInternalServerError, &out)
			}

		}

	}

	out := pdbresponse.SetResponse(true, "Succesfully PostParameterItems", &pics)
	return c.JSON(http.StatusOK, &out)

}

func DeleteItemPicture(c echo.Context) error {

	picID := c.QueryParam("picID")
	pic := new(pdbitem.Picture)

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Unscoped().Where("id = ?", &picID).Find(&pic).Delete(&pic).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error DeleteItemPicture", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully DeleteItemPicture", &pic)
	return c.JSON(http.StatusOK, &out)

}
