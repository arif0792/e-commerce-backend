package pdbparameter

import (
	"ALKA-backend/db/pdbcon"
	"ALKA-backend/pdbresponse"
	"ALKA-backend/pdbstruct/pdbsetting"
	"fmt"
	"net/http"

	"github.com/labstack/echo"
)

func GetParameterSettingLebaranMonth(c echo.Context) error {
	setting := new(pdbsetting.Setting)

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Where("setting_name = 'LebaranMonth'").Find(&setting).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetParameterSettingLebaranMonth", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetParameterSettingLebaranMonth", &setting)
	return c.JSON(http.StatusOK, &out)

}

func PostParameterSetting(c echo.Context) error {
	setting := new(pdbsetting.Setting)

	if err := c.Bind(&setting); err != nil {
		out := pdbresponse.SetResponse(false, "Error Data Binding", err.Error())
		fmt.Println("Error => " + err.Error())
		return c.JSON(http.StatusBadRequest, &out)
	}

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Model(&setting).Where("setting_name = ?", &setting.SettingName).Update("setting_value", &setting.SettingValue).Error; err != nil {
		out := pdbresponse.SetResponse(false, "Error PostParameterSetting", err)
		fmt.Println("Error => " + err.Error())
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully PostParameterSetting", &setting)
	return c.JSON(http.StatusOK, &out)

}

func GetParameterSettingDeadlineMonth(c echo.Context) error {
	setting := new(pdbsetting.Setting)

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Where("setting_name = 'DeadlineMonth'").Find(&setting).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetParameterSettingDeadlineMonth", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetParameterSettingDeadlineMonth", &setting)
	return c.JSON(http.StatusOK, &out)

}

func GetParameterSettingBannerPics(c echo.Context) error {
	setting := new(pdbsetting.Setting)

	db := pdbcon.Connecting()
	defer db.Close()

	if err := db.Where("setting_name = 'BannerPicIds'").Find(&setting).GetErrors(); len(err) > 0 {
		out := pdbresponse.SetResponse(false, "Error GetParameterSettingBannerPics", err)
		return c.JSON(http.StatusInternalServerError, &out)
	}

	out := pdbresponse.SetResponse(true, "Succesfully GetParameterSettingBannerPics", &setting)
	return c.JSON(http.StatusOK, &out)

}
