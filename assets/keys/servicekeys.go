package keys

const (
	//ENV PROD or DEV
	Env = "DEV"
	//Own Address DEV
	OwnAddress = "http://192.168.0.16:1323"
	//Own Address PROD
	// OwnAddress = "https://gentle-thicket-39624.herokuapp.com/"
	//DB
	DBType         = "mysql"
	DBAddressLocal = "root:1234@/alka?charset=utf8&parseTime=True&loc=Local"
	DBAddress      = "b755445ad35737:9b6c7f9d@tcp(us-cdbr-iron-east-05.cleardb.net)/heroku_c61c96650c11017?charset=utf8&parseTime=True&loc=Local"
	//dev duitku
	Duitku_url          = "https://sandbox.duitku.com/webapi/api/merchant/v2/inquiry"
	Duitku_merchantKey  = "5b22eee1f0234bd6b1f511d726502bfe"
	Duitku_merchantCode = "D3811"
	//prod duitku
	Duitku_url_prod          = "https://passport.duitku.com/webapi/api/merchant/v2/inquiry"
	Duitku_merchantKey_prod  = "627e6529e1fa87699440b9ac73dfeb44"
	Duitku_merchantCode_prod = "D0628"
	//
	Duitku_callbackUrl = OwnAddress + "/panen/v1/paymentgateway/callback"
	Duitku_returnUrl   = OwnAddress
	FcmServerKey       = "AAAAJQKoaPo:APA91bEo4usKB0_NcxfToLLo5wBSKdcPq1M2fY2tfWKuCAHq375cq3DHe9IkEZXjcp-CF4LXGM5_u-LObzOZhX-JSPLCGZeTunFvRQwaIxkHN3P5-ZJfxubJIJHOapsYzZyY9CgYtOMw"
)
