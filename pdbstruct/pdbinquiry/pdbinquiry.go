package pdbinquiry

import "github.com/jinzhu/gorm"

type Inquiry struct {
	gorm.Model
	From string `json:"from"`
	Data string `json:"data"`
}
