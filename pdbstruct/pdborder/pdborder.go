package pdborder

import (
	"ALKA-backend/pdbstruct/pdbpayment"
	"ALKA-backend/pdbstruct/pdbproduct"
	decimal "ALKA-backend/shopspring-decimal"
	"time"

	"github.com/jinzhu/gorm"
)

type Order struct {
	gorm.Model
	TotalInstallment    int             `json:"totalInstallment"`
	TotalPrice          decimal.Decimal `json:"totalPrice" sql:"type:decimal(20,8);"`
	Status              int             `json:"status"`
	IsRefund            bool            `json:"isRefund"`
	OrderDate           time.Time       `json:"orderDate"`
	CanceledDate        *time.Time      `json:"canceledDate"`
	CompletedDate       *time.Time      `json:"completedDate"`
	UserID              int             `json:"userId"`
	HaveCancelDeadline  bool            `json:"haveCancelDeadline"`
	IsActive            bool            `json:"isActive"`
	DeadlineCancelMonth int             `json:"deadlineCancelMonth"`
	LebaranMonth        int             `json:"lebaranMonth"`

	Products       []pdbproduct.Product       `json:"products"`
	PaymentDetails []pdbpayment.PaymentDetail `json:"paymentDetails"`
}
