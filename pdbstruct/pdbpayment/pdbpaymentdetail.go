package pdbpayment

import (
	"ALKA-backend/pdbstruct/pdbduitkuDetail"

	"github.com/jinzhu/gorm"
)

type PaymentDetail struct {
	gorm.Model
	PayMethod         string `json:"payMethod"`
	InstallmentNumber int    `json:"installmentNumber"`
	OrderID           uint   `json:"orderId"`

	DuitkuDetail *pdbduitkuDetail.DuitkuDetail `json:"paymentDuitkuDetail"`
}
