package pdbduitkuDetail

import (
	decimal "ALKA-backend/shopspring-decimal"

	"github.com/jinzhu/gorm"
)

// DuitkuDetail Detail
type DuitkuDetail struct {
	gorm.Model
	RefID             string          `json:"refID"`
	ResultCode        string          `json:"resultCode"`
	VaNumber          *string         `json:"vaNumber"`
	PaymentMethod     string          `json:"paymentMethod"`
	PaymentAmount     decimal.Decimal `json:"paymentAmount" sql:"type:decimal(20,8);"`
	InstallmentNumber int             `json:"installmentNumber"`
	OrderID           uint            `json:"orderId"`
	PaymentDetailID   uint            `json:"paymentDetailId"`
}
