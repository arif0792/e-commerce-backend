package pdbsetting

import "github.com/jinzhu/gorm"

type Setting struct {
	gorm.Model
	SettingName  string `json:"settingName"`
	SettingValue string `json:"settingValue"`
}
