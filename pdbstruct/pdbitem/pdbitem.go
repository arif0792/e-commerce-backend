package pdbitem

import (
	decimal "ALKA-backend/shopspring-decimal"

	"github.com/jinzhu/gorm"
)

type Item struct {
	gorm.Model
	ItemPrice          decimal.Decimal `json:"itemPrice" sql:"type:decimal(20,8);"`
	ItemAdminPrice     decimal.Decimal `json:"itemAdminPrice" sql:"type:decimal(20,8);"`
	ItemDeliveryPrice  decimal.Decimal `json:"itemDeliveryPrice" sql:"type:decimal(20,8);"`
	ItemCategory       string          `json:"itemCategory"`
	ItemName           string          `json:"itemName"`
	HaveCancelDeadline bool            `json:"haveCancelDeadline"`
	ItemPictureIds     string          `json:"itemPictureIds"`
}

type Picture struct {
	gorm.Model
	Data []byte `json:"data" sql:"type:LONGBLOB;"`
}
