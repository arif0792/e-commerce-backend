package pdbuser

import (
	"ALKA-backend/pdbstruct/pdbaddress"
	"ALKA-backend/pdbstruct/pdborder"

	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`
	Email       string `json:"email" gorm:"not null;unique"`
	PhoneNumber string `json:"phoneNumber" gorm:"not null;unique"`
	UserName    string `json:"userName" gorm:"not null;unique"`
	Password    string `json:"password"` //udah ga valid, real password after reset not valid, real password on firebase
	FirebaseUID string `json:"firebaseUID"`
	FcmToken    string `json:"fcmToken"`

	Orders          []pdborder.Order     `json:"orders"`
	Addresses       []pdbaddress.Address `json:"addresses"`
	Messages        []Message            `json:"messages"`
	Reads           []Read               `json:"reads"`
	AdminPrivileges *AdminPrivilege      `json:"adminPrivileges"`
}

type Message struct {
	gorm.Model
	From string `json:"from"`
	Data string `json:"data"`

	UserID int `json:"userId"`
}

type Read struct {
	gorm.Model
	Read      bool `json:"read"`
	MessageID int  `json:"messageId"`

	UserID int `json:"userId"`
}

type AdminPrivilege struct {
	gorm.Model
	DashboardUser     bool `json:"dashboardUser"`
	DashboardPotong   bool `json:"dashboardPotong"`
	DashboardDelivery bool `json:"dashboardDelivery"`
	DashboardRefund   bool `json:"dashboardRefund"`
	DashboardOverdue  bool `json:"dashboardOverdue"`
	DashboardHistory  bool `json:"dashboardHistory"`
	BroadcastMessage  bool `json:"broadcastMessage"`
	Inquiry           bool `json:"inquiry"`
	Settings          bool `json:"settings"`

	UserID int `json:"userId"`
}
