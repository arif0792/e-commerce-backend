package pdbproduct

import (
	decimal "ALKA-backend/shopspring-decimal"

	"github.com/jinzhu/gorm"
)

type Product struct {
	gorm.Model
	ProductID       string          `json:"productId"`
	Status          int             `json:"status"`
	AlamatID        int             `json:"alamatId"`
	OrderID         uint            `json:"orderId"`
	ItemName        string          `json:"itemName"`
	ItemPrice       decimal.Decimal `json:"itemPrice" sql:"type:decimal(20,8);"`
	ItemAdminPrice  decimal.Decimal `json:"itemAdminPrice" sql:"type:decimal(20,8);"`
	ItemOptionPrice decimal.Decimal `json:"itemOptionPrice" sql:"type:decimal(20,8);"`
	ItemOption      string          `json:"itemOption"`
}
