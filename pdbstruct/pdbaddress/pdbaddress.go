package pdbaddress

import (
	"github.com/jinzhu/gorm"
)

// Address from user
type Address struct {
	gorm.Model
	AddressAlias string `json:"addressAlias"`
	Address      string `json:"address"`
	PostalCode   int    `json:"postalCode"`
	HomeNo       string `json:"homeNo"`
	Kelurahan    string `json:"kelurahan"`
	UserID       uint   `json:"userId"`
}
